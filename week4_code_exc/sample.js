function doIt(){
var input1 = document.getElementById('number1').value
var input2 = document.getElementById('number2').value

var answer = Number(input1)+Number(input2)

document.getElementById('answer').innerHTML = answer

if(answer<0){
    document.getElementById('answer').className="positive"
    document.getElementById('answer').color = "blue"
}
    else {
        document.getElementById('answer').className = "negative"
        document.getElementById('answer').color ="red"
    }
              
if (answer % 2 === 0){
    document.getElementById('discription').className = "even"
    document.getElementById('discription').innerHTML ="(even)"
    document.getElementById('discription').color ="purple"
}
    else {
        document.getElementById('discription').className = "odd"
        document.getElementById('discription').innerHTML ="(odd)"
        document.getElementById('discription').color ="orange"
    }
}